module.exports = {
  render: function () {
    var $footerMenu = $('#section-footer ul.footer-menu').clone();
    $('#section-footer .container-full').append($footerMenu);
    $('#section-footer .row.footer-holder').remove();
  }
};