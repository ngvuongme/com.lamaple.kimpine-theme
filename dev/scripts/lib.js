var baseURL = '';
var data = null;
if (document.location.hostname === 'localhost') {
  var getUrl = window.location;
  baseURL = getUrl.protocol + '//' + getUrl.host + '/' + getUrl.pathname.split('/')[1];
} else {
  baseURL = 'http://' + location.host;
}

var getJsonData = $.getJSON(baseURL+'/PF.Site/flavors/kimpine/assets/data.json', function(json) {
  data = json;
});

$.fn.inlineStyle = function (prop) {
  var styles = this.attr('style'),value;
  styles && styles.split(';').forEach(function (e) {
    var style = e.split(':');
    if ($.trim(style[0]) === prop) {
      value = style[1];
    }
  });
  return value;
};

$.fn.exchangePositionWith = function(selector) {
  var other = $(selector);
  this.after(other.clone());
  other.after(this).remove();
};

module.exports = {
  getData: function  () {
    return getJsonData;
  },

  getPageURL: function () {
    return baseURL;
  }
};