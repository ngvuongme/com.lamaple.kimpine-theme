var lib = require('./lib');
var page = require('./page');
var landing = require('./landing');
var home = require('./home');
var footer = require('./footer');
var header = require('./header');
var navbar = require('./navbar');
var post = require('./post');

var baseURL = lib.getPageURL();

header.render(baseURL);
navbar.render();
page.render();
footer.render();

lib.getData().done(function (json) {
  if (page.getName() === 'landing') {
    landing.render(json.kimpine, baseURL);
    landing.bindingForm();
  }

  if (page.getName() === 'home') {
    post.render();
  }
});