module.exports = {
  render: function () {
    var self = this;
    var posts = $('#js_feed_content .js_feed_view_more_entry_holder');

    posts.each(function (e) {
      self.moveImgOnTop(this);
      self.addStyleToPost(this);
      self.moveTimeStamp(this);
    });
  },

  addStyleToPost: function (post) {
    $(post).addClass('single-post');
  },

  moveTimeStamp: function (post) {
    $(post).find('.js_mini_feed_comment').each(function (e) {
      var selectItems = $(this).find('.comment_mini_action ul > li:lt(2)');

      if (selectItems.length > 0) {
        $(selectItems).addClass('foo');
        var $htmlString = '<span class="time-stamp">';
        $(this).find('li.foo').each(function (e) {
          $htmlString += $(this).html();
        });
        $(post).find ('');
        $htmlString += '</span>';

        $(this).find('.comment_mini_content .user_profile_link_span').append($htmlString);
      }
    });
    $(post).find('li.foo').each(function (e) {
      $(this).remove();
    });
  },

  moveImgOnTop: function (post) {
    var imgSrc = $(post).find('.activity_feed_content_image > a img').attr('data-src');
    $(post).find('img').attr('src', imgSrc);
    var $imgEl = $(post).find('.activity_feed_content_image > a').clone();
    $(post).find('.activity_feed_content_image').remove();
    $imgEl.addClass('post-photo');
    
    $(post).find('.js_user_feed').prepend($imgEl);
  }
};