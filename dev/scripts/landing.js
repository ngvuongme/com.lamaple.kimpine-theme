module.exports = {
  render: function (json, baseURL) {
    var labels = json.landingPage.labels,
      links = json.landingPage.links;

    var $menuItem = $('ul.nav.site_menu.visible-md.visible-lg li:first-child');
    $('.welcome-message h2').text(labels.welcomeMessage).css('left',$menuItem.outerWidth(true));
    $('#js_block_border_user_register > .title').addClass('display-none').text(labels.signUp);
    $('#js_block_border_user_register > .title').after($('<a class="login-btn btn-active">'+labels.logIn+'</a>'));
    $('#js_register_step1').after($('<p>I have agree the <a href="'+ links.termUrl + '">'+ labels.termOfUse +'</a> and <a href="'+ links.privacyUrl + '">'+ labels.privacyPolicy +'</a></p>'));

    $('#js_registration_holder').addClass('login');
    $('#js_form').after($('<form class="content login-form active" method="post" action="'+ baseURL + '/index.php/user/login/" id="js_login_form" onsubmit=""> <div class="table form-group"> <div class="table_right"> <input class="form-control" placeholder="Email" type="email" name="val[login]" id="login" value="" size="40"> </div> <div class="clear"></div> </div> <div class="table form-group"> <div class="table_right"> <input class="form-control" placeholder="Password" type="password" name="val[password]" id="password" value="" size="40"> </div> <div class="clear"></div> </div> <div class="table_clear form-group"> <div class="p_top_base checkbox"> <ul class="clearfix"> <li><input type="checkbox" name="val[remember_me]" value=""><label><span></span>Remember me</label></li> <li><a class="no_ajax" href="'+ baseURL + '/index.php/user/password/request/">Forgot your password?</a></li> </ul> </div> <button type="submit" class="button btn-primary text-uppercase"> '+labels.signIn+' </button> </div> <input type="hidden" name="val[parent_refresh]" value="1"> </form>'));
  },

  bindingForm: function () {
    var $signupForm = $('#js_registration_holder #js_form'),
      $loginForm = $('#js_registration_holder #js_login_form'),
      $btnLogin = $('.login-btn'),
      $btnSignup = $('#js_block_border_user_register > .title'),
      $formWrapper = $('#js_registration_holder');

    $btnLogin.click(function () {
      $btnLogin.addClass('btn-active');
      $loginForm.addClass('active');
      $btnSignup.removeClass('btn-active');
      $signupForm.removeClass('active');
      $formWrapper.removeClass('signup');
      $formWrapper.addClass('login');
    });

    $btnSignup.click(function () {
      $btnSignup.addClass('btn-active');
      $signupForm.addClass('active');
      $btnLogin.removeClass('btn-active');
      $loginForm.removeClass('active');
      $formWrapper.removeClass('login');
      $formWrapper.addClass('signup');
    });
  }
};