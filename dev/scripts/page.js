module.exports = {
  render: function () {
    $('#main > .row_image .row:nth-child(2)').addClass('container-full row-container');

    setTimeout(
      function() 
      {
        $('body').addClass('active');
      }, 25);
  },
  
  getName: function () {
    var pageId = $('body').attr('id');
    if (pageId === 'page_core_index-visitor') {
      return 'landing';
    } else if (pageId === 'page_core_index-member') {
      return 'home';
    } 
    return -1;
  }
};