module.exports = {
  render: function () {
    var self = this;
    self.setItemWidth();
    self.windowResize();
  },

  setItemWidth: function () {
    var $listItems = $('ul.nav.site_menu.visible-md.visible-lg'),
      count = $listItems.children().length,
      width = 0;

    $listItems.children().each(function (e) {
      width += $(this).width();
    });

    var itemWidth = ($listItems.width() - width) / (count);
    $listItems.children().each(function (e) {
      $(this).css('margin-right', itemWidth+itemWidth/(count*2));
    });
  },

  windowResize: function () {
    var self = this;
    // Screen size >= 992
    if ($(window).width() >= 992) {
      $(window).resize(function() {
        self.setItemWidth();
      });
    }
  }
};