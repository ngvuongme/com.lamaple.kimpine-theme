module.exports = {
  render: function (baseURL) {
    if (!$('#section-header .site-logo-icon i').inlineStyle('background-image')) {
      $('#section-header .site-logo-icon i').css('background-image', 'url('+baseURL+'/PF.Site/flavors/kimpine/assets/images/kim-pine-logo.png)');
    }
  }
};