// Gulp Dependencies
var gulp = require('gulp');
var rename = require('gulp-rename');

// Build Dependencies
var browserify = require('gulp-browserify');
var uglify = require('gulp-uglify');

// Style Dependencies
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');

// Development Dependencies
var jshint = require('gulp-jshint');

// Browserify tasks
gulp.task('lint', function() {
  return gulp.src('scripts/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});
gulp.task('browserify', ['lint'], function() {
  return gulp.src('scripts/main.js')
    .pipe(browserify({
      insertGlobals: true
    }))
    .pipe(rename('kimpine.js'))
    .pipe(gulp.dest('build'));
});

// Build tasks
gulp.task('styles', function() {
  return gulp.src('sass/style.scss')
  .pipe(sass({onError: function(e) { console.log(e); } }))
  .pipe(autoprefixer({browsers:['last 2 versions'],cascade: true}))
  .pipe(rename('kimpine.css'))
  .pipe(gulp.dest('build/'));
});
gulp.task('minify', ['styles'], function() {
  return gulp.src('build/kimpine.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(rename('autoload.css'))
    .pipe(gulp.dest('../assets/'));
});
gulp.task('uglify', ['browserify'], function() {
  return gulp.src('build/kimpine.js')
    .pipe(uglify())
    .pipe(rename('autoload.js'))
    .pipe(gulp.dest('../assets/'));
});


// Build task
gulp.task('build', ['minify', 'uglify']);

// Watch task
gulp.task('watch', ['build'], function() {
  gulp.watch(['sass/**'], ['minify']);
  gulp.watch(['scripts/**'], ['uglify']);
});

// Dev task
gulp.task('dev', function() {
  gulp.run('watch');
});
